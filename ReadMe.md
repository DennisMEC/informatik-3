
F�r Syntax und Designm�glichkeiten dieser Datei siehe: https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html 	
	
	
# Allgemeine Informationen

Das ist die Readme f�r das Informatik-Projekt aus dem Modul Informatik 3 im Sommersemester 2017 an der Hochschule 21.

Das Ziel des Projektes ist es, eine graphische Benutzeroberfl�che(GUI) f�r die Studentendatenbank zu realisieren.
Es muss in der Kalenderwoche 37 abgegeben werden.

Mit dabei sind:

Sabrina Kart�usch
Matthias Dittmann
Marcel Dammann
Patrick Zimmermann
Dennis Eutin


# Dokumentation des Quellcodes
	
## 1.	Benennung der Variablen nach camelCase


		Beispiel: int nutzerAuswahl;	string zeitZaehler
		Der Anfang des zusammengesetzten Namens wird klein geschrieben, wohingegen der Anfang eines eigenst�ndigen ""Unterwortes"" gro� geschrieben wird
		Beispiel: wandUhr; camelCase; taschenRechner
		
		
		
## 2.	Ausf�hrliche Kommentierung der Funktionsk�pfe


		Jede Funktion bekommt einen "Kommentarkopf", der die Fukntion mit ihren Eigenschaften erkl�rt. Hier ist eine Kopier-Vorlage daf�r, wichtig ist der dreifache Backslash
		und auch <summary> und co.
	
	
		//----------------------------------------
		
		///
		/// <summary>
		/// Was macht diese Funktion?
		/// </summary>
		///
		/// <param name="Name des ersten Parameters">Beschreibung des ersten Parameters </param>
		/// <param name="Name des zweiten Parameters">Beschreibung des zweiten Parameters </param>
		///
		/// <returns>
		///  Was gibt diese Funktion inhaltlich zur�ck?
		/// </returns>
		///  
		/// <created> Wer hat das wann erstellt? </created>
		/// <changed> Wer hat es wann zuletzt ge�ndert? </changed>
		
		//----------------------------------------
		
		
		Dieser Kopf hat insofern den Vorteil, dass VisualStudio diesen erkennt und immer dann, wenn man eine Funtkion mit einem solchen Kopf aufruft,
		eine Erkl�rung/Tooltipp aus den gemachten Angaben bereitstellt. Das liefert Transparenz, wenn Funktionen aufgerufen werden sollen,
		die man selbst nicht programmiert hat.
		
		
		
## 3.	Inline Kommentare im Programmablauf

		
		Das sind die altbekannten Kommentare, die mit einem Doppelslash // eingeleitet werden. Diese bitte im Verlauf einer Funktion einsetzen, um zu erkl�ren,
		was gerade aus welchem Grund gemacht wird, sodass es jeder eindeutig verstehen kann. Bitte darauf achten, dass die Kommentare den gleichen Einr�ckungsgrad hat,
		wie die zu beschreibenden Zeilen.
		Zum Beispiel:
		
		// deklarieren der einzelnen Variablen
		// �ffnen eines neuen Threads
		// �bergeben des Funktionswertes
		
		
		
## 4.	Codeeinr�ckungen und Zeilenl�ngen


		Bitte sinnvoll Code einr�cken und die Zeilen nicht unendlich lang werden lassen, der Code soll immerhin �berschaubar und �bersichtlich bleiben.
		
	

## 5.	Automatisch generierten Code Kennzeichnen

		
		Sollte es vorkommen, dass Code von Visual Studio automatisch generiert wurde, wenn zum Beispiel Fenster ertsellt werden, dann bitte immer den automatisch 
		generierten Code wie folgt kennzeichnen.
		
		//-------------- Automatisch generierter Code Anfang --------------
		
		//-------------- Automatisch generierter Code Ende --------------
		
		So wisse wir, welche Bereiche im Code f�r uns wichtig sein k�nnten, wenn wir nach Fehlern suchen oder bestimmte Variablen und Parameter
		
		